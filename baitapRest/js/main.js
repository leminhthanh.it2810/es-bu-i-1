tinhDTB = ((...e) => {
    let n = 0;
    return e.map(e => {
        n += parseFloat(e)
    }
    ),
        (n / e.length).toFixed(2)
}
),
    document.getElementById("btnKhoi1").onclick = (() => {
        let diemToan = document.getElementById("inpToan").value
            , diemLy = document.getElementById("inpLy").value
            , diemHoa = document.getElementById("inpHoa").value;
        document.getElementById("tbKhoi1").innerHTML = tinhDTB(diemToan, diemLy, diemHoa)
    }
    ),
    document.getElementById("btnKhoi2").onclick = (() => {
        let diemVan = document.getElementById("inpVan").value
            , diemSu = document.getElementById("inpSu").value
            , diemDia = document.getElementById("inpDia").value
            , diemTiengAnh = document.getElementById("inpEnglish").value;
        document.getElementById("tbKhoi2").innerHTML = tinhDTB(diemVan, diemSu, diemDia, diemTiengAnh)
    }
    );